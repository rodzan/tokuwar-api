const express = require('express')

const controller = require('../controllers/users')

module.exports = (app) => {
    app.get('/users', controller.get)
    app.get('/user/:id', controller.getById)
    app.post('/post-user/', controller.getById) 
}