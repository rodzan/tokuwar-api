const mongoose = require('mongoose')
const schemma = mongoose.Schema

const users = new schemma(
    {
        id: String,
        nome: String,
        email: String,
        senha: String,
        username: String,
        cep: Number,
        endereco: String,
        bairro: String,
        cidade: String,
        pais: String
    },
    { collection: 'users' }
)

const User = mongoose.model('User', users)

module.exports = {
    Mongoose: mongoose,
    User: users
}