const express = require('express');
const bodyParser = require('body-parser')
const multiparty = require('connect-multiparty')
const consign = require('consign')

module.exports = () => {
    let app = express()

    app.use(express.static('./app/public'));

    app.use(bodyParser.urlencoded({ extended:true}));
    app.use(bodyParser.json());
    app.use(multiparty());

    app.use((req, res, next) => {

        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
        res.setHeader("Access-Control-Allow-Headers", "content-type");
        res.setHeader("Access-Control-Allow-Credentials", true);

        next();
    });

    consign()
    .include('config/db.js')
    .then('app/models')
    .then('app/controllers')
    .then('app/routes')
    .into(app)

    return app
}